import {ApiPathsEnum} from "@app/enums/apiPaths";
import {ApiDescriptionEnum} from "@app/enums/apiDescription";
import {ApiMessagesEnum} from "@app/enums/apiMessages";


export const apiPathsEnum = ApiPathsEnum;
export const apiDescriptionEnum = ApiDescriptionEnum;
export const apiMessagesEnum = ApiMessagesEnum;
