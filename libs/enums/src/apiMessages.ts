export enum ApiMessagesEnum {
    unauthorized = 'Unauthorized',
    cannotCreate = 'Cannot be created'
}
