export enum ApiDescriptionEnum {
  moviesCreate = 'Endpoint responsible for creating a movie object based on movie title. ' +
    "<br>Endpoint require authorization by passed in request's header: Authorization: Bearer <token>" +
    "<br>Users with role 'premium' have unlimited access to this endpoint, but users with role 'basic' " +
      "can use it five times. Limit resets first day of every month at 0:00 " +
    "<br>Movies titles cannot duplicate" +
    "<br>It takes one value in request's body:" +
    '<li>"name": Movie name</li>' +
    '<br><br>Response is a newly created movie object',
  moviesShow = 'Endpoint responsible for displaying all movies created by an authorized user' +
    "<br>Endpoint require authorization by passed in request's header: Authorization: Bearer <token>" +
    '<br>Response is an array of movies objects',
  login = 'Endpoint responsible for user login to the system. ' +
    "<br>It takes two values in request's body:" +
    '<li>"username": User\'s account name as string ( min length: 5, max length: 50 )</li>' +
    '<li>"password": user password as string ( min length: 5, max length: 50 )<li>' +
    '<br><br>After a successful login, a JsonWebToken is returned',
}
