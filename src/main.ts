import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';
import {ConfigService} from '@nestjs/config';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    const configService = app.get(ConfigService);

    const swaggerOptions = new DocumentBuilder()
        .setTitle('Swagger - Netguru recruitment task')
        .setDescription(' ')
        .setVersion('1.0')
        .addBearerAuth({
            type: 'http',
            scheme: 'bearer',
            bearerFormat: 'jwt',
        })
        .build();
    const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
    SwaggerModule.setup('api', app, swaggerDocument, {
        swaggerOptions: {
            docExpansion: 'none',
        },
    });
    await app.listen(configService.get('APP_PORT') || 8081);
}

bootstrap();
