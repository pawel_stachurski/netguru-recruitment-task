import {CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable,} from '@nestjs/common';
import {Reflector} from '@nestjs/core';
import {AuthService} from './auth.service';
import {apiMessagesEnum} from "@app/enums";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private authService: AuthService, private reflector: Reflector) {
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        // const permissionInMetadata = this.reflector.get<string[]>('roles', context.getHandler());
        const authorizationHeader: string = context.switchToHttp().getRequest()
            .headers.authorization;
        if (authorizationHeader === undefined) {
            throw new HttpException(apiMessagesEnum.unauthorized, HttpStatus.UNAUTHORIZED);
        }
        const roleInToken = await this.authService.verifyUserRole(
            authorizationHeader,
        );
        if (roleInToken === 'basic' || roleInToken === 'premium') {
            return true;
        } else {
            return false;
        }
        // return await this.authService.verifyPermissions(roleInToken, permissionInMetadata);
    }
}
