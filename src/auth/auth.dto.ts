import {ApiProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsString, MaxLength, MinLength} from 'class-validator';

export class LoginDto {
  @ApiProperty({example: 'premium-jim'})
  @IsString()
  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(50)
  username: string;

  @ApiProperty({example: 'GBLtTyq3E_UNjFnpo9m6'})
  @IsString()
  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(50)
  password: string;
}
