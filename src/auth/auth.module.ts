import {Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {AuthController} from './auth.controller';
import {RolesGuard} from './roles.guard';
import {SequelizeModule} from '@nestjs/sequelize';
import {UsersModel} from './users.model';

@Module({
    imports: [SequelizeModule.forFeature([UsersModel])],
    controllers: [AuthController],
    providers: [AuthService, RolesGuard],
    exports: [AuthService],
})
export class AuthModule {
}
