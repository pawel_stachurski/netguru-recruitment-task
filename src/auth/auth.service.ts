import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import {LoginDto} from './auth.dto';
import {InjectModel} from '@nestjs/sequelize';
import {UsersModel} from './users.model';
import {Cron} from '@nestjs/schedule';
import {apiMessagesEnum} from "@app/enums";

@Injectable()
export class AuthService {
    constructor(
        @InjectModel(UsersModel) private usersRepository: typeof UsersModel,
    ) {
    }

    async signPayload(payload, userId: number) {
        return jwt.sign(payload, process.env.JWT_SECRET, {
            issuer: 'https://www.netguru.com/',
            subject: `${userId}`,
            expiresIn: 30 * 60,
        });
    }

    async verifyUserRole(token): Promise<string> {
        if (!token) {
            throw new HttpException(apiMessagesEnum.unauthorized, HttpStatus.UNAUTHORIZED);
        }
        try {
            return jwt.verify(
                token.slice(7, 300),
                process.env.JWT_SECRET,
                function (err, decoded) {
                    return decoded.role;
                },
            );
        } catch (err) {
            throw new HttpException(apiMessagesEnum.unauthorized, HttpStatus.UNAUTHORIZED);
        }
    }

    async verifyUser(token): Promise<string> {
        if (!token) {
            throw new HttpException(apiMessagesEnum.unauthorized, HttpStatus.UNAUTHORIZED);
        }
        try {
            return jwt.verify(
                token.slice(7, 300),
                process.env.JWT_SECRET,
                function (err, decoded) {
                    return decoded;
                },
            );
        } catch (err) {
            throw new HttpException(apiMessagesEnum.unauthorized, HttpStatus.UNAUTHORIZED);
        }
    }

    async reduceLimit(headers) {
        const decodedToken: any = await this.verifyUser(headers.authorization);
        const user = await this.usersRepository.findOne({
            where: {
                id: decodedToken.userId,
            },
        });

        if (user.role === 'basic') {
            user.moviesCreationLimit = user.moviesCreationLimit - 1;
            await user.save();
        }
    }

    async canCreateMovie(headers): Promise<boolean> {
        const decodedToken: any = await this.verifyUser(headers.authorization);
        if (decodedToken.role === 'premium') {
            return true;
        }
        if (decodedToken.role === 'basic') {

            const user = await this.usersRepository.findOne({
                where: {id: decodedToken.userId},
                attributes: ['moviesCreationLimit']
            });
            return user.moviesCreationLimit > 0;

        } else {
            return false;
        }
    }

    async findByLogin(loginDto: LoginDto): Promise<UsersModel> {

        const user = await this.usersRepository.findOne({
            where: {
                username: loginDto.username,
            },
        });

        if (!user || user.password !== loginDto.password) {
            throw new HttpException(apiMessagesEnum.unauthorized, HttpStatus.UNAUTHORIZED);
        }
        return user;
    }

    @Cron('0 0 1 * *')
    async resetMoviesCreationLimit() {

        return this.usersRepository.update({
            moviesCreationLimit: 5
        }, {
            where: {}
        })
    }
}
