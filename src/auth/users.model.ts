import {Column, DataType, Model, PrimaryKey, Table,} from 'sequelize-typescript';

@Table({tableName: 'users'})
export class UsersModel extends Model<UsersModel> {
    @PrimaryKey
    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        primaryKey: true,
        unique: true,
    })
    id: number;

    @Column({
        type: DataType.STRING,
        defaultValue: 'basic',
        allowNull: false,
        unique: false,
    })
    role: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
        unique: false,
    })
    name: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
    })
    username: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: false,
    })
    password: string;

    @Column({
        type: DataType.INTEGER,
        defaultValue: 5,
        allowNull: false,
        unique: false,
    })
    moviesCreationLimit: number;
}
