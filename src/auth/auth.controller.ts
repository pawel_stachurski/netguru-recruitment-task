import {Body, Controller, Post} from '@nestjs/common';
import {AuthService} from './auth.service';
import {LoginDto} from './auth.dto';
import {apiDescriptionEnum, apiPathsEnum} from "@app/enums";
import {ApiOperation} from "@nestjs/swagger";

@Controller(apiPathsEnum.auth)
export class AuthController {
    constructor(private authService: AuthService) {
    }

    @Post()
    @ApiOperation({description: apiDescriptionEnum.login})
    async login(@Body() loginDto: LoginDto) {
        const user = await this.authService.findByLogin(loginDto);
        const payload = {
            userId: user.id,
            name: user.name,
            role: user.role,
            limit: user.moviesCreationLimit,
        };
        const token = await this.authService.signPayload(payload, user.id);
        return {token};
    }

}
