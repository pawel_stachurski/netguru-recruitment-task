import {HttpException, HttpService, HttpStatus, Injectable,} from '@nestjs/common';
import {CreateMovieDto, MoviesDto} from './movies.dto';
import {InjectModel} from '@nestjs/sequelize';
import {MoviesModel} from './movies.model';
import {AuthService} from '../auth/auth.service';
import {apiMessagesEnum} from "@app/enums";

@Injectable()
export class MoviesService {
    constructor(
        @InjectModel(MoviesModel) private moviesRepository: typeof MoviesModel,
        private httpService: HttpService,
        private authService: AuthService,
    ) {
    }

    async createMovie(dto: CreateMovieDto, headers): Promise<MoviesDto> {
        const canCreate : boolean = await this.authService.canCreateMovie(headers);
        if (canCreate === false) {
            throw new HttpException(apiMessagesEnum.cannotCreate, HttpStatus.FORBIDDEN);
        }

        const fetchedData = await this.getMovieData(dto.name);
        const preparedMovieObject : MoviesDto = MoviesDto.createMovieFactory(fetchedData.data);
        try {
        await this.moviesRepository.create(preparedMovieObject);
            await this.authService.reduceLimit(headers);
        } catch (error) {
            return error.message
        }

        return preparedMovieObject;
    }

    async getMovieData(name: string) {
        return await this.httpService
            .get(`http://www.omdbapi.com/?` +
                `apikey=${process.env.OMDBADI_API_KEY}` +
                `&t=${name}`)
            .toPromise();
    }

    async showMovies(): Promise<MoviesModel[]> {
        return this.moviesRepository.findAll({
            attributes: {exclude: ['id', 'createdAt', 'updatedAt']},
            order: [['title', 'ASC']],
            raw: true,
        });
    }
}
