import {Body, Controller, Get, Post, Request, UseGuards,} from '@nestjs/common';
import {MoviesService} from './movies.service';
import {CreateMovieDto} from './movies.dto';
import {ApiBearerAuth, ApiOperation} from '@nestjs/swagger';
import {RolesGuard} from '../auth/roles.guard';
import {apiDescriptionEnum, apiPathsEnum} from "@app/enums";

@ApiBearerAuth()
@Controller(apiPathsEnum.movies)
export class MoviesController {
    constructor(private moviesService: MoviesService) {
    }

    @UseGuards(RolesGuard)
    @ApiOperation({description: apiDescriptionEnum.moviesCreate})
    @Post()
    async createMovie(@Body() dto: CreateMovieDto, @Request() request: Request) {
        return await this.moviesService.createMovie(dto, request.headers);
    }

    @UseGuards(RolesGuard)
    @ApiOperation({description: apiDescriptionEnum.moviesShow})
    @Get()
    async showMovies() {
        return await this.moviesService.showMovies();
    }
}
