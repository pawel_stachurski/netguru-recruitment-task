import {HttpModule, Module} from '@nestjs/common';
import {MoviesService} from './movies.service';
import {MoviesController} from './movies.controller';
import {SequelizeModule} from '@nestjs/sequelize';
import {MoviesModel} from './movies.model';
import {AuthModule} from '../auth/auth.module';

@Module({
    imports: [SequelizeModule.forFeature([MoviesModel]), HttpModule, AuthModule],
    controllers: [MoviesController],
    providers: [MoviesService],
})
export class MoviesModule {
}
