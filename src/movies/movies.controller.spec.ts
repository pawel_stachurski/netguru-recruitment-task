import * as request from 'supertest';
import {HttpStatus} from '@nestjs/common';
import {CreateMovieDto} from './movies.dto';
const faker = require('faker');

describe('MoviesController', () => {
    const localApi = 'http://localhost:8081';
    let token = null;

    beforeAll(function (done) {
        request(localApi)
            .post('/auth')
            .send({
                username: 'premium-jim',
                password: 'GBLtTyq3E_UNjFnpo9m6',
            })
            .end(function (err, res) {
                token = res.body.token;
                done();
            });
    });

    it('Add movie without authorization', () => {
        const movie: CreateMovieDto = {
            name: faker.name.firstName()
        };
        return request(localApi)
            .post('/movies')
            .send(movie)
            .expect(HttpStatus.UNAUTHORIZED);
    });

    it('Add movie as authorized user', () => {
        const movie: CreateMovieDto = {
            name: faker.name.firstName()
        };

        return request(localApi)
            .post('/movies')
            .set({Authorization: `Bearer ${token}`})
            .send(movie)
            .expect(HttpStatus.CREATED);
    });

    it('Show movies for without authorization', () => {
        return request(localApi).get('/movies').expect(HttpStatus.UNAUTHORIZED);
    });

    it('Show movies as authorized user', () => {
        return request(localApi)
            .get('/movies')
            .set({Authorization: `Bearer ${token}`})
            .expect(HttpStatus.OK);
    });
});
