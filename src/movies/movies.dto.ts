import {ApiProperty} from '@nestjs/swagger';
import {IsNotEmpty, IsString} from 'class-validator';

export class CreateMovieDto {
    @ApiProperty({example: 'Batman'})
    @IsString()
    @IsNotEmpty()
    name: string;
}

export class MoviesDto {
    title: string;
    released: Date;
    genre: string;
    director: string;

    static createMovieFactory(fetchedData) {
        const movieObject = new MoviesDto();

        movieObject.title = fetchedData.Title;
        movieObject.released = new Date(fetchedData.Released);
        movieObject.genre = fetchedData.Genre;
        movieObject.director = fetchedData.Director;

        return movieObject;
    }
}
