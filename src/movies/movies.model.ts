import {Column, DataType, Model, PrimaryKey, Table,} from 'sequelize-typescript';

@Table({tableName: 'movies'})
export class MoviesModel extends Model<MoviesModel> {
    @PrimaryKey
    @Column({
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
        allowNull: false,
        primaryKey: true,
        unique: true,
    })
    id: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: true,
    })
    title: string;

    @Column({
        type: DataType.DATE,
        allowNull: true,
        unique: false,
    })
    released: Date;

    @Column({
        type: DataType.STRING,
        allowNull: true,
        unique: false,
    })
    genre: string;

    @Column({
        type: DataType.STRING,
        allowNull: true,
        unique: false,
    })
    director: string;
}
