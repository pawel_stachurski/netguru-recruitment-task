'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {

    const users = [
      {
        id: 123,
        role: 'basic',
        name: 'Basic Thomas',
        username: 'basic-thomas',
        password: 'sR-_pcoow-27-6PAwCD8',
        moviesCreationLimit: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 434,
        role: 'premium',
        name: 'Premium Jim',
        username: 'premium-jim',
        password: 'GBLtTyq3E_UNjFnpo9m6',
        moviesCreationLimit: 5,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ];
    return await queryInterface.bulkInsert('users', users)
  },

  down: async (queryInterface, Sequelize) => {

    return await queryInterface.renameTable('users', 'users_old')
  }
};
