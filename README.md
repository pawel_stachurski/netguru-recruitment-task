## Description

Netguru recruitment task by Paweł Stachurski.

API for fetching movie details based on passed movie title and saving it to database.

Tech stack:
- Typescript
- framework: NestJs
- database: Postgres
- ORM: Sequelize
- Sequelize seeder
- Swagger (documentation)

## Installation

Api is dockerized, so you only need to have docker and docker-compose installed on your computer.
All environment variables are stored in .env file.

## Running the app

```bash
# Building app
$ docker-compose build 

# Running database, application and seeder (to insert test accounts to DB)
$ docker-compose up
```
Application should start on port 8081, db on port 5001.

## API Documentation

Endpoints documentation is available in swagger, after you start application, on http://localhost:8081/api/ 

```bash
#### GET /movies
Endpoint responsible for displaying all movies created by an authorized user
Endpoint require authorization by passed in request's header: Authorization: Bearer
Response is an array of movies objects

#### POST /movies
Endpoint responsible for creating a movie object based on movie title.
Endpoint require authorization by passed in request's header: Authorization: Bearer.
Users with role 'premium' have unlimited access to this endpoint, but users with role 'basic'
can use it five times each month. Limit resets first day of every month at 0:00.
Movies titles cannot duplicate.
It takes one value in request's body:
- "name": Movie name
Response is a newly created movie object

#### POST /auth
Endpoint responsible for user login to the system.
It takes two values in request's body:
"username": User's account name as string ( min length: 5, max length: 50 )
"password": user password as string ( min length: 5, max length: 50 )
After a successful login, a JsonWebToken is returned
```
